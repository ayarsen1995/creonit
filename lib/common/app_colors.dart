import 'package:flutter/material.dart';

class AppColors{
  static const Color primaryBlack = Color(0xff414951); 
  static const Color red = Color(0xffE30613); 
  static const Color grayColor = Color(0xff8A8884); 
  static const Color grayColorTransparent = Color(0x8f8A8884); 
  static const Color grayColorFullTransparent = Color(0x148A8884); 
  static const Color greenColorHit = Color(0xff537B39); 
}